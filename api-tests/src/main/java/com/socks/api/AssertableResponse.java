package com.socks.api;

import com.socks.api.conditions.Condition;
import com.socks.api.util.CustomObjectMapper;
import com.socks.api.util.logging.Loggable;
import io.qameta.allure.Step;
import io.restassured.response.ValidatableResponse;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class AssertableResponse{

    private final ValidatableResponse validatableResponse;

    @Step("api response should have {condition}")
    @Loggable
    public AssertableResponse shouldHave(Condition condition){
        condition.check(validatableResponse);
        return this;
    }

    @Step("mapping response body to class {cls}")
    @Loggable
    public <T> T jsonToPojo(Class<T> cls){
        return validatableResponse.extract().body().as(cls);
    }

    @Step("mapping response body to class {cls}")
    @Loggable
    public <T> T textToPojo(Class<T> cls){
        return CustomObjectMapper.map(validatableResponse.extract().body().asString(), cls);
    }

}
