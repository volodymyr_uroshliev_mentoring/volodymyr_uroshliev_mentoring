package com.socks.api.conditions;

import io.restassured.response.ValidatableResponse;
import lombok.AllArgsConstructor;
import org.hamcrest.Matcher;

@AllArgsConstructor
public class BodyCondition implements Condition {
    private String jsonPath;
    private Matcher<?> matcher;

    @Override
    public void check(ValidatableResponse response) {
        response.assertThat().body(jsonPath, matcher);
    }

    @Override
    public String toString() {
        return "json element by path " + jsonPath + " satisfy matcher " + matcher.toString();
    }
}
