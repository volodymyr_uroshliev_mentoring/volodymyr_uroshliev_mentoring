package com.socks.api.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.Accessors;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class CardRequest {
    @JsonProperty("longNum")
    private String longNum;
    @JsonProperty("expires")
    private String expires;
    @JsonProperty("ccv")
    private String ccv;
    @JsonProperty("userID")
    private String userId;
}
