package com.socks.api.model.response.addresses;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.socks.api.model.response.customer.Links;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.Objects;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Address {
    @JsonProperty("street")
    private String street;
    @JsonProperty("number")
    private String number;
    @JsonProperty("country")
    private String country;
    @JsonProperty("city")
    private String city;
    @JsonProperty("postcode")
    private String postcode;
    @JsonProperty("id")
    private String id;
    @JsonProperty("_links")
    private Links links;
}
