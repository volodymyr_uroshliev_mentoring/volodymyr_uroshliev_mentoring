package com.socks.api.model.response.addresses;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class AddressEmbedded {
    @JsonProperty("address")
    private List<Address> addresses;
}
