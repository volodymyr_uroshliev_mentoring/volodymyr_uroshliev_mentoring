package com.socks.api.model.response.addresses;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class AddressesResponse {
    @JsonProperty("_embedded")
    private AddressEmbedded embedded;
    @JsonProperty("_links")
    private Object links;
    @JsonProperty("page")
    private Object page;
}
