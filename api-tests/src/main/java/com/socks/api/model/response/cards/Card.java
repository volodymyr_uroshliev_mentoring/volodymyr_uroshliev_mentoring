package com.socks.api.model.response.cards;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.socks.api.model.response.SelfLink;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.Map;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Card {
    @JsonProperty("longNum")
    private String longNum;
    @JsonProperty("expires")
    private String expires;
    @JsonProperty("ccv")
    private String ccv;
    @JsonProperty("_links")
    private SelfLink links;
    @JsonProperty("card")
    private Map<String, String> card;
}
