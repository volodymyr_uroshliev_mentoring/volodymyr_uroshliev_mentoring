package com.socks.api.model.response.cards;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.Accessors;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class CardResponse {
    @JsonProperty("_embedded")
    private CardsEmbedded embedded;
    @JsonProperty("_links")
    private Object links;
    @JsonProperty("page")
    private Object page;
}
