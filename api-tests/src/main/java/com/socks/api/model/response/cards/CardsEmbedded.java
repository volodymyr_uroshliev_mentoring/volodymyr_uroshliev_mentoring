package com.socks.api.model.response.cards;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class CardsEmbedded {
    @JsonProperty("card")
    private List<Card> card;
}
