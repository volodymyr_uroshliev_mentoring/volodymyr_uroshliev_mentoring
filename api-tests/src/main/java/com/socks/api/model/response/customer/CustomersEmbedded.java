package com.socks.api.model.response.customer;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class CustomersEmbedded {
    @JsonProperty("customer")
    private List<Customer> customers;
}
