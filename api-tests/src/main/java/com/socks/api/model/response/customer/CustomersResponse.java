package com.socks.api.model.response.customer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.Accessors;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomersResponse {
    @JsonProperty("_embedded")
    private CustomersEmbedded embedded;
    @JsonProperty("_links")
    private Object links;
    @JsonProperty("page")
    private Object page;
}
