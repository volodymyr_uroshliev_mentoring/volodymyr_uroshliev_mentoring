package com.socks.api.model.response.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.socks.api.model.response.AddressesLink;
import com.socks.api.model.response.CardsLink;
import com.socks.api.model.response.SelfLink;
import lombok.*;
import lombok.experimental.Accessors;

import static com.fasterxml.jackson.annotation.JsonInclude.*;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Links {
    @JsonProperty("self")
    private SelfLink self;
    @JsonProperty("customer")
    private CustomerLink customer;
    @JsonProperty("addresses")
    private AddressesLink addresses;
    @JsonProperty("cards")
    private CardsLink cards;
    @JsonProperty("address")
    private AddressesLink address;
}
