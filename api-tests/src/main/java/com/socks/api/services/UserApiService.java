package com.socks.api.services;

import com.socks.api.AssertableResponse;
import com.socks.api.conditions.Conditions;
import com.socks.api.model.request.AddressRequest;
import com.socks.api.model.request.User;
import com.socks.api.util.HttpServiceClient;
import com.socks.api.util.logging.Loggable;
import io.qameta.allure.Step;
import io.restassured.http.Cookie;
import lombok.Getter;
import lombok.Setter;
import org.apache.http.HttpStatus;

@Getter
@Setter
public class UserApiService extends HttpServiceClient {
    public static final String LOGIN_COOKIE_NAME = "logged_in";

    private static final String CUSTOMERS_URL = "/customers";
    private static final String REGISTER_URL = "/register";
    private static final String LOGIN_URL = "/login";
    private static final String ADDRESSES_URL = "/addresses";

    @Step
    @Loggable
    public AssertableResponse registerUser(User user, int expectedStatus) {
        return new AssertableResponse(postRequest(REGISTER_URL, user))
            .shouldHave(Conditions.statusCode(expectedStatus));
    }

    @Step
    @Loggable
    public AssertableResponse getCustomers(int expectedStatus) {
        return new AssertableResponse(getRequest(CUSTOMERS_URL))
            .shouldHave(Conditions.statusCode(expectedStatus));
    }

    @Step
    @Loggable
    public AssertableResponse getCustomer(String id, Cookie loginCookie, int expectedStatus) {
        return new AssertableResponse(getRequest(CUSTOMERS_URL + "/" + id, loginCookie))
                .shouldHave(Conditions.statusCode(expectedStatus));
    }

    @Step
    @Loggable
    public AssertableResponse deleteUser(String id) {
        return new AssertableResponse(deleteRequest(CUSTOMERS_URL + "/" + id))
            .shouldHave(Conditions.statusCode(HttpStatus.SC_OK));
    }

    //We need to extract login cookie value so don't see
    //the way to return assertable response
    @Step
    @Loggable
    public String login(String userName, String password, int expectedStatus) {
        return new AssertableResponse(getWithBasicAuth(LOGIN_URL, userName, password))
                .shouldHave(Conditions.statusCode(expectedStatus))
                .getValidatableResponse()
                .extract().cookie(LOGIN_COOKIE_NAME);
    }

    @Step
    @Loggable
    public AssertableResponse addAddress(AddressRequest request, Cookie loginCookie) {
        return new AssertableResponse(postRequest(ADDRESSES_URL, request, loginCookie));
    }

    @Step
    @Loggable
    public AssertableResponse getAddresses(int expectedStatus) {
        return new AssertableResponse(getRequest(ADDRESSES_URL))
                .shouldHave(Conditions.statusCode(expectedStatus));
    }

    @Step
    @Loggable
    public AssertableResponse deleteAddress(String id) {
        return new AssertableResponse(deleteRequest(ADDRESSES_URL + "/" + id))
                .shouldHave(Conditions.statusCode(HttpStatus.SC_OK));
    }
}
