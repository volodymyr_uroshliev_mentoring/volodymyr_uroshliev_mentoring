package com.socks.api.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.socks.api.util.logging.Loggable;
import lombok.extern.slf4j.Slf4j;


import java.io.IOException;

//Custom deserializer for response from '/customers' endpoint
//as RestAssured can't parse content with 'plain/text' content type header

@Slf4j
public class CustomObjectMapper {
    @Loggable
    public static <T> T map(String stringToParse, Class<T> clazz) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(stringToParse, clazz);
        } catch (IOException e) {
            log.error("unable to parse string {} to class {}", stringToParse, clazz);
            e.printStackTrace();
        }
        return null;
    }
}
