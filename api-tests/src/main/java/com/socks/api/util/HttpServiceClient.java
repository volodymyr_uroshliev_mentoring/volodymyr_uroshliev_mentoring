package com.socks.api.util;

import com.socks.api.util.configuration.Application;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.http.Cookie;
import io.restassured.http.Cookies;
import io.restassured.http.Header;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class HttpServiceClient {
    //To hold request specification context in different requests
    private RequestSpecification requestSpecification;

    //Just more readable than do it with setter in test
    public void invalidateRequestSpecification(){
        setRequestSpecification(null);
    }

    public RequestSpecification requestSetup(List<Header> headers){

        if(requestSpecification != null){
            return requestSpecification;
        }

        requestSpecification = RestAssured.given()
                .contentType(ContentType.JSON);

        if(Application.CONFIG.getConfig().requestLogs())
        {
            requestSpecification.filters(new RequestLoggingFilter(),
                new ResponseLoggingFilter(),
                new AllureRestAssured());
        } else {
            requestSpecification.filters(new AllureRestAssured());
        }

        if(headers != null && !headers.isEmpty()){
            headers.forEach(requestSpecification::header);
        }

        return requestSpecification;
    }

    public ValidatableResponse postRequest(String path, Object requestPayload){
        return requestSetup(null).body(requestPayload).when().post(path).then();
    }

    public ValidatableResponse postRequest(String path, Object requestPayload, Cookie ... cookie){
        if (cookie != null && cookie[0] != null){
            Cookies cookies = new Cookies(cookie);
            requestSetup(null).cookies(cookies);
        }
        return requestSetup(null).body(requestPayload).when().post(path).then();
    }

    public ValidatableResponse getRequest(String path){
        return requestSetup(null).when().get(path).then();
    }

    public ValidatableResponse getRequest(String path, Cookie ... cookie){
        if (cookie != null && cookie[0] != null){
            Cookies cookies = new Cookies(cookie);
            requestSetup(null).cookies(cookies);
        }
        return requestSetup(null).when().get(path).then();
    }

    public ValidatableResponse deleteRequest(String path){
        return requestSetup(null).when().delete(path).then();
    }

    public ValidatableResponse getWithBasicAuth(String path, String userName, String password){
        return requestSetup(null).auth().preemptive().basic(userName, password).when().get(path).then();
    }
}
