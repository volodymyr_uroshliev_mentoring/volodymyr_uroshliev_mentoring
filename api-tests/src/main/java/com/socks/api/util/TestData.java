package com.socks.api.util;

import com.github.javafaker.Faker;
import com.socks.api.model.request.AddressRequest;
import com.socks.api.model.request.User;
import com.socks.api.util.logging.Loggable;
import io.qameta.allure.Step;

public class TestData {
    private Faker faker;

    //setup default faker params(no params actually)
    public TestData() {
        faker = new Faker();
    }

    public Faker getFaker() {
        return faker;
    }

    @Step
    @Loggable
    public User getDefaultUser() {
        return new User()
                .setFirstName(faker.name().firstName())
                .setLastName(faker.name().lastName())
                .setPassword("password") //it is done intentionally to avoid headache with passwords
                .setEmail(faker.internet().emailAddress())
                .setUsername(faker.name().name().replaceAll(" ", "")
                        + faker.random().nextInt(100500));
    }

    @Step
    @Loggable
    public User getOnlyRequiredParamsUser() {
        return new User()
                .setPassword("password") //it is done intentionally to avoid headache with passwords
                .setEmail(faker.internet().emailAddress())
                .setUsername(faker.name().name().replaceAll(" ", "")
                        + faker.random().nextInt(100500));
    }

    @Step
    @Loggable
    public AddressRequest getAddress() {
        return new AddressRequest()
                .setStreet(faker.address().streetName())
                .setNumber(faker.address().buildingNumber())
                .setCountry(faker.address().country())
                .setCity(faker.address().cityName())
                .setPostcode(faker.address().zipCode());
    }
}
