package com.socks.api.util.configuration;

import org.aeonbits.owner.Config;

public interface AppConfig extends Config {

    @Key("base.url")
    @DefaultValue("http://104.248.73.177")
    String baseUrl();

    @Key("request.logs")
    @DefaultValue("false")
    Boolean requestLogs();

    @Key("method.logs")
    @DefaultValue("true")
    Boolean methodLogs();
}
