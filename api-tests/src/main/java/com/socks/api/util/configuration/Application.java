package com.socks.api.util.configuration;

import org.aeonbits.owner.ConfigFactory;

public enum Application {
    CONFIG;

    public AppConfig getConfig(){
        return ConfigFactory.create(AppConfig.class,
                System.getProperties(),
                System.getenv());
    }
}
