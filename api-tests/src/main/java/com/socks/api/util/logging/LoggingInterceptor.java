package com.socks.api.util.logging;

import com.socks.api.util.configuration.Application;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Aspect
@Slf4j
public class LoggingInterceptor {

    @Around("execution(* *(..)) && @annotation(Loggable)")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        final MethodSignature methodSignature = (MethodSignature) point.getSignature();
        final String methodName = point.getArgs().length > 0
                ? String.format("%s (%s)", methodSignature.getMethod().getName(), arrayToString(point.getArgs())) // -> (1)
                : methodSignature.getMethod().getName() + "()";

        Object result = point.proceed();
//        if(System.getProperty("method.logs").equals(true)){
        if(Application.CONFIG.getConfig().methodLogs()){
            log.info(methodName);
        }
        return result;
    }

    private static String arrayToString(final Object... array) {
        return Stream.of(array)
                .map(object -> {
                    if (object.getClass().isArray()) {
                        return arrayToString((Object[]) object);
                    }
                    return Objects.toString(object);
                })
                .collect(Collectors.joining(", "));
    }
}
