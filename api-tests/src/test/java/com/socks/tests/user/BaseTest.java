package com.socks.tests.user;

import com.socks.api.util.TestData;
import com.socks.api.util.configuration.Application;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeAll;

public class BaseTest {
    public static final String HASH_PATTERN = "[a-z0-9]{24}";

    protected TestData testData = new TestData();

    @BeforeAll
    public static void setUp() {
        RestAssured.baseURI = Application.CONFIG.getConfig().baseUrl();
    }
}
