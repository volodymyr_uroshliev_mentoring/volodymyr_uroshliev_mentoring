package com.socks.tests.user;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import com.socks.api.model.request.User;
import com.socks.api.model.response.CreateResponse;
import com.socks.api.services.UserApiService;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LoginTest extends BaseTest{

    private UserApiService userApiService;
    private String id;
    private User user;

    @BeforeEach
    public void before(){
        userApiService = new UserApiService();
        user = testData.getDefaultUser();
        id = userApiService.registerUser(user, 200)
            .jsonToPojo(CreateResponse.class).getId();
    }

    @AfterEach
    public void cleanUp() {
        if(id != null && !id.isEmpty()) {
            userApiService.deleteUser(id);
        }
    }

    @Test
    public void testUserCanLogin(){
        String loginTokenPattern = "[a-zA-Z0-9_-]{32}";
        String loginToken = userApiService.login(user.getUsername(), user.getPassword(), HttpStatus.SC_OK);
        //don't want to add specific matcher for this case
        assertThat(loginToken, not(is(emptyString())));
        assertThat(loginToken, matchesPattern(loginTokenPattern));
    }

    @Test
    public void testUserCantLoginWithoutPassword(){
        userApiService.login(user.getUsername(), "", HttpStatus.SC_UNAUTHORIZED);
    }

    @Test
    public void testUserCantLoginWithoutUserName(){
        userApiService.login("", user.getPassword(), HttpStatus.SC_UNAUTHORIZED);
    }
}
