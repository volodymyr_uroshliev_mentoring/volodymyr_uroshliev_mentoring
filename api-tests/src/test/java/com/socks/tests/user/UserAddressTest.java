package com.socks.tests.user;

import com.socks.api.AssertableResponse;
import com.socks.api.conditions.Conditions;
import com.socks.api.model.request.AddressRequest;
import com.socks.api.model.request.User;
import com.socks.api.model.response.CreateResponse;
import com.socks.api.model.response.addresses.Address;
import com.socks.api.model.response.addresses.AddressesResponse;
import com.socks.api.services.UserApiService;
import com.socks.api.util.logging.Loggable;
import io.qameta.allure.Step;
import io.restassured.http.Cookie;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


public class UserAddressTest extends BaseTest{

    private UserApiService userApiService;
    private String id;
    private Cookie loginCookie;
    private String addressId;

    @BeforeEach
    public void before(){
        userApiService = new UserApiService();
        User user = testData.getDefaultUser();
        AssertableResponse response = userApiService.registerUser(user, 200);
        loginCookie = response.getValidatableResponse().extract().response().detailedCookie(UserApiService.LOGIN_COOKIE_NAME);
        id = response.jsonToPojo(CreateResponse.class).getId();
    }

    @AfterEach
    public void cleanUp() {
        if(id != null && !id.isEmpty()) {
            userApiService.deleteUser(id);
        }

        if(addressId != null && !addressId.isEmpty()){
            userApiService.deleteAddress(addressId);
        }
    }

    @Test
    public void testAddressCanBeAdded(){
        AddressRequest addressRequest = testData.getAddress().setUserId(id);
        addressId = userApiService.addAddress(addressRequest, loginCookie)
            .shouldHave(Conditions.statusCode(HttpStatus.SC_OK))
            .textToPojo(CreateResponse.class).getId();

        AddressesResponse addressesResponse = userApiService.getAddresses(HttpStatus.SC_OK)
                .textToPojo(AddressesResponse.class);
        Address actualAddress = findAddress(addressesResponse, addressId);
        verifyAddressesIdentical(addressRequest, actualAddress);
    }

    private Address findAddress(AddressesResponse addressesResponse, String addressId){
        return addressesResponse.getEmbedded().getAddresses().stream()
            .filter(address -> address.getId().equals(addressId))
            .findFirst()
            .get();
    }

    @Step
    @Loggable
    private void verifyAddressesIdentical(AddressRequest expected, Address actual){
        assertThat(expected.getCountry(), is(actual.getCountry()));
        assertThat(expected.getCity(), is(actual.getCity()));
        assertThat(expected.getStreet(), is(actual.getStreet()));
        assertThat(expected.getNumber(), is(actual.getNumber()));
        assertThat(expected.getPostcode(), is(actual.getPostcode()));
    }

    //Cannot finish test-suite due to different errors from api :(
}
