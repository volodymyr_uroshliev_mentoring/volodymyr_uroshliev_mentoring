package com.socks.tests.user;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import com.socks.api.conditions.Conditions;
import com.socks.api.model.response.customer.Customer;
import com.socks.api.model.response.customer.CustomersResponse;
import com.socks.api.model.request.User;
import com.socks.api.model.response.CreateResponse;
import com.socks.api.services.UserApiService;
import com.socks.api.util.logging.Loggable;
import io.qameta.allure.Step;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;


public class UserTest extends BaseTest{

    private UserApiService userApiService = new UserApiService();
    private String id;

    @AfterEach
    public void cleanUp() {
        if(id != null && !id.isEmpty()) {
            userApiService.deleteUser(id);
        }
    }

    @Test
    public void createUserWithAllParams() {
        User user = testData.getDefaultUser();
        CreateResponse createResponse = userApiService.registerUser(user, 200)
                .shouldHave(Conditions.body("id", not(is(emptyString()))))
                .shouldHave(Conditions.body("id", matchesPattern("[a-z0-9]{24}")))
                .jsonToPojo(CreateResponse.class);
        id = createResponse.getId();
        verifyCreatedUser(createResponse.getId(),
            userApiService.getCustomers(200).textToPojo(CustomersResponse.class),
            user);
    }

    @Test
    public void createUserWithOnlyRequiredParams() {
        User user = testData.getOnlyRequiredParamsUser();
        CreateResponse createResponse = userApiService.registerUser(user, 200)
                .shouldHave(Conditions.body("id", not(is(emptyString()))))
                .shouldHave(Conditions.body("id", matchesPattern(HASH_PATTERN)))
                .jsonToPojo(CreateResponse.class);
        id = createResponse.getId();
        verifyCreatedUser(createResponse.getId(),
                userApiService.getCustomers(200).textToPojo(CustomersResponse.class),
                user);
    }

    @Test
    public void userShouldNotBeCreatedWithEmptyPayload() {
        User user = new User();
        userApiService.registerUser(user, 500);
    }

    @Test
    public void userShouldNotBeCreatedWithoutUsername() {
        User user = testData.getOnlyRequiredParamsUser().setUsername(null);
        userApiService.registerUser(user, 500);
    }

    @Step
    @Loggable
    public void verifyCreatedUser(String id, CustomersResponse customersResponse, User user) {
        Customer customer = customersResponse.getEmbedded().getCustomers().stream()
                .filter(custom -> custom.getId().equals(id))
                .findFirst()
                .get();

        assertThat(user.getUsername(), is(equalTo(customer.getUsername())));

        if(user.getFirstName() != null) {
            assertThat(user.getFirstName(), is(equalTo(customer.getFirstName())));
        }

        if(user.getLastName() != null) {
            assertThat(user.getLastName(), is(equalTo(customer.getLastName())));
        }
    }
}
